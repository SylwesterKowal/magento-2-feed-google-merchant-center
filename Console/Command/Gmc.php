<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 23/11/2018
 * Time: 19:47
 */

namespace M21\FeedGmc\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class Gmc extends Command
{

    const NAME_ARGUMENT = "name";
    const NAME_OPTION = "option";

    public function __construct(
        \Magento\Framework\App\State $state,
        \M21\FeedGmc\lib\Feed $feed,
        $name = null
    )
    {
        $this->state = $state;
        $this->feed = $feed;
        parent::__construct($name);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    )
    {
        try {
            if (!$this->state->getAreaCode()) {
                $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML); // or \Magento\Framework\App\Area::AREA_FRONTEND
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        $name = $input->getArgument(self::NAME_ARGUMENT);
        $option = $input->getOption(self::NAME_OPTION);
        $output->writeln("Start " . $name);

        $this->feed->run();

        $output->writeln("Koniec " . $name);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("m21_feedgmc:gmc");
        $this->setDescription("Testowe generowanie feedu do GMC");
        $this->setDefinition([
            new InputArgument(self::NAME_ARGUMENT, InputArgument::OPTIONAL, "Name"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }
}