<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 15/11/2018
 * Time: 00:09
 */

namespace M21\FeedGmc\lib;

use Magento\Framework\Exception\LocalizedException;

class Feed
{
    public function __construct(
        \Magento\Framework\App\State $state,
        \M21\FeedGmc\lib\Settings $settings,
        \M21\FeedGmc\lib\Queries $queries,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\CatalogInventory\Helper\Stock $stockFilter,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Filesystem\DirectoryList $directoryList,
//        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Tax\Api\TaxCalculationInterface $taxCalculation,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurable,
        \Magento\Framework\Escaper $escaper,
        $name = null
    )
    {
        $this->state = $state;
        $this->settings = $settings;
        $this->escaper = $escaper;
        $this->queries = $queries;
        $this->collectionFactory = $collectionFactory;
        $this->taxCalculation = $taxCalculation;
        $this->scopeConfig = $scopeConfig;
        $this->stockFilter = $stockFilter;
        $this->stockRegistry = $stockRegistry;
        $this->configurable = $configurable;
//        $this->connection = $resourceConnection->getConnection();
        $this->storeManager = $storeManager;
        $this->currentStore = $this->storeManager->getStore();
        $this->baseUrl = $this->currentStore->getBaseUrl();
        $this->mediaDir = str_replace('pub/', '', $this->currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA)) . 'catalog/product';
        $this->pubDir = $directoryList->getPath('pub');
        $delimiter = ['{d}' => "\t"];
        $this->head = str_replace(
            array_keys($delimiter),
            array_values($delimiter),
            $this->settings->getHead()
        );;
        $this->stringContent = $this->settings->getContent();
        $this->footer = $this->settings->getFooter();

    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        try {
            if (!$this->state->getAreaCode()) {
                $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML); // or \Magento\Framework\App\Area::AREA_FRONTEND
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        $store_id = 1;
        $my_file = $this->getFile();


        $handle = fopen($my_file, 'a') or die('Cannot open file:  ' . $my_file);

        fwrite($handle, $this->head . PHP_EOL);

        $collection = $this->collectionFactory->create()
            ->addStoreFilter($store_id)
            ->addAttributeToFilter('visibility', [2, 3, 4])
            ->addAttributeToFilter('gmc_export', 1)
            ->addAttributeToFilter('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED)
//            ->addAttributeToFilter('price', ['gt' => 0])
            ->addAttributeToSelect('*');

        $collection->joinField(
            'qty', 'cataloginventory_stock_item', 'qty', 'product_id=entity_id', '{{table}}.stock_id=1', 'left'
        );
//        $this->stockFilter->addInStockFilterToCollection($collection);

        foreach ($collection as $product) {

            if ($product->getTypeId() == 'configurable') {
//                $data = $this->getDataArray($product);
//                fwrite($handle, $data . PHP_EOL);
                $child_products = $product->getTypeInstance()->getUsedProducts($product, null);
                if (count($child_products) > 0) {
                    foreach ($child_products as $child) {
                        if ($data = $this->getDataArray($child, $product)) {
                            fwrite($handle, $data . PHP_EOL);
                        }
                    }
                }
            } else {
                if ($data = $this->getDataArray($product)) {
                    fwrite($handle, $data . PHP_EOL);
                }
            }
        }
//        fwrite($handle, $this->footer);
        fclose($handle);
    }


    private function getDataArray($product, $parent = null)
    {
        if ($parent) {
            $stockItem = $this->stockRegistry->getStockItem($product->getId());
            $item_group_id = $parent->getId();
            $avil = ($stockItem->getQty() >= 1) ? 'in stock' : 'out of stock';
            $url = $parent->getProductUrl();
        } else {
            $avil = ($product->getQty() >= 1) ? 'in stock' : 'out of stock';
            $item_group_id = '';
            $url = $product->getProductUrl();
        }

        if ($product->getTypeId() == 'simple' && $avil == 99) return false;

        $oznaczeniekoloru = $product->getResource()->getAttribute('color')->getFrontend()->getValue($product);
        $price = round($this->getMinPrice($product), 2);
        $brand = $this->removeTabs(trim($product->getResource()->getAttribute('manufacturer')->getFrontend()->getValue($product)));
        $oznaczeniekoloru = $this->removeTabs(trim($oznaczeniekoloru));
        $productName = $this->removeTabs(trim($product->getName()));

        $replace = [
            '{d}' => "\t",
            '{id}' => $product->getEntityId(),
            '{item_group_id}' => $item_group_id,

            '{title}' => $productName,
            '{description}' => $this->removeTabs($product->getDescription()),
            '{link}' => /* $this->baseUrl .*/
                $url,
            '{image_link}' => $this->mediaDir . $product->getImage(),
            '{availability}' => $avil,
            '{additional_image_link}' => '',
            '{price}' => $price,
            '{sale_price}' => '',
            '{sale_price_effective_date}' => '',
            '{brand}' => $brand,
            '{mpn}' => $this->removeTabs(trim($product->getSku())),
            '{condition}' => 'new',
            '{product_type}' => $this->getProductType($product),
            '{google_product_category}' => $this->getMapedCategoryPath($product),
            '{identifier_exists}' => '',
            '{is_bundle}' => 'FALSE',
            '{shipping}' => '',
            '{shipping_weight}' => round($product->getWeight(), 2),
            '{promotions_id}' => '',
            '{gtin}' => $this->removeTabs($product->getEan()),
            '{color}' => $oznaczeniekoloru
        ];
        if ((!is_null($parent))) {
            $superAttrId = $this->queries->getSuperAttribiutId($parent->getEntityId());
            $replace['{link}'] = $replace['{link}'] . '#' . $superAttrId . '=' . $this->queries->getAttribiutValue($superAttrId, $product->getEntityId());
        }
        return str_replace(
            array_keys($replace),
            array_values($replace),
            $this->stringContent
        );
    }

    private function removeTabs($data)
    {
        $data = strip_tags($data);
        return trim(preg_replace('/\s+/', ' ', html_entity_decode($data)));
    }

    private function getFile()
    {
        $folder = (!empty($this->settings->getFolder())) ? $this->settings->getFolder() : 'gmcfeed';
        $filename = (!empty($this->settings->getFileName())) ? $this->settings->getFileName() : 'gmcfeed.txt';
        $path = $this->pubDir . DIRECTORY_SEPARATOR . $folder;
        $gmc_file = $path . DIRECTORY_SEPARATOR . $filename;
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        if (file_exists($gmc_file)) unlink($gmc_file);
        return $gmc_file;
    }

    /**
     * Pobranie Kategorii pod Product_type
     * @param $product
     * @param bool $seria
     * @return string
     */
    private function getProductType($product)
    {
        $store = 1;
        $categoryCollection = clone $product->getCategoryCollection();
        $categoryCollection->clear();
        $categoryCollection->addAttributeToSort('level', $categoryCollection::SORT_ORDER_DESC)
            ->addAttributeToFilter('path', array('like' => "1/" . $this->storeManager->getStore($store)->getRootCategoryId() . "/%"));


        foreach ($categoryCollection as $category) {
            $gmc_product_type = $this->queries->getCategoryAttribiutValue('gmc_product_type', $category->getId());
            if ($gmc_product_type == 1) {
                $breadcrumbCategories = $category->getParentCategories();
                $breadcrumbs_serie = [];
                foreach ($breadcrumbCategories as $category_) {
                    $breadcrumbs_serie[] = $category_->getName();
                }
                if (!empty($breadcrumbs_serie)) $path_serie[] = implode(' > ', $breadcrumbs_serie);
            }
        }

        if (!empty($path_serie)) return implode(', ', $path_serie);
    }

    private function getMapedCategoryPath($product)
    {
        $store = 1;
        $categoryCollection = clone $product->getCategoryCollection();
        $categoryCollection->clear();
        $categoryCollection->addAttributeToSort('level', $categoryCollection::SORT_ORDER_DESC)
            ->addAttributeToFilter('path', array('like' => "1/" . $this->storeManager->getStore($store)->getRootCategoryId() . "/%"));
//        $categoryCollection->setPageSize(1);

        $map = [];
        $first = false; // tylko najniższy mapowany poziom
        foreach ($categoryCollection as $category) {
            if ($catGMC = $this->queries->getCategoryAttribiutValue('kategoria_gmc', $category->getId(), 'catalog_category_entity_varchar')) {
                if (!$first) {
                    $map[$catGMC] = $catGMC;
                    $first = true;
                }
            }
        }
        return implode(', ', $map);
    }


    private function getMinPrice($product)
    {
        try {
            if ($product->getTypeId() == 'configurable') {
                $child_products = $product->getTypeInstance()->getUsedProducts($product, null);
                if (count($child_products) > 0) {
                    $i = 1;
                    foreach ($child_products as $child) {
                        if ($i == 1) $price = $this->getMinPriceValue($child);
                        $productPrice = $this->getMinPriceValue($child);
                        $price = $price ? max($price, $productPrice) : $productPrice;
                        $i++;
                    }
                }
                return $price;
            } else {
                $price = $this->getMinPriceValue($product);
                return $price;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    private function getMinPriceValue($product)
    {
        try {
            $rate = $this->taxCalculation->getCalculatedRate($product->getTaxClassId());
            $sPrice = (float)$product->getSpecialPrice();
            $rPrice = (float)$product->getPrice();
            if ($sPrice != 0 && $sPrice < $rPrice) {
                $price = $sPrice;
            } else {
                $price = $rPrice;
            }
            if ((int)$this->scopeConfig->getValue(
                    'tax/calculation/price_includes_tax',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE) === 1
            ) {
                // Product price in catalog is including tax.
                return $price;
            } else {
                // Product price in catalog is excluding tax.
                return $price + ($price * ($rate / 100));
            }
            return $rPrice;

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}