<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 27.09.2018
 * Time: 12:33
 */

namespace M21\FeedGmc\lib;


class Settings
{
    protected $scopeConfig;

    /**
     * Settings constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\State $state,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->state = $state;
        try {
            $state = $this->state->getAreaCode(); // getAreaCode() wywoła Exception jesli nie jest ustawiony
        } catch (\Exception $e) {
            $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML); // or \Magento\Framework\App\Area::AREA_FRONTEND
//            echo $e->getMessage();
        }
    }

    public function getStatus()
    {
        return $this->scopeConfig->getValue('gmc_status/xml/enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getHead()
    {
        return $this->scopeConfig->getValue('gmc_ustawienia/xml/head', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getContent()
    {
        return $this->scopeConfig->getValue('gmc_ustawienia/xml/content', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getFooter()
    {
        return $this->scopeConfig->getValue('gmc_ustawienia/xml/footer', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getFolder()
    {
        return $this->scopeConfig->getValue('gmc_status/xml/folder', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getFileName()
    {
        return $this->scopeConfig->getValue('gmc_status/xml/file_name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}